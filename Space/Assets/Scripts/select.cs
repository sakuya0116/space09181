﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class select : MonoBehaviour
{
    public Camera m_Camera;

    public GameObject ScanPanel;
    public GameObject CollectionPanel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = m_Camera.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            if (Physics.Raycast(ray, out hit))
            {
                GameObject obj = hit.collider.gameObject;
                if (obj.name == "pCylinder40")
                {
                    ScanPanel.SetActive(true);
                }

                if (obj.name == "pCylinder50")
                {
                    ScanPanel.SetActive(true);
                }

                if (obj.name == "pCylinder28")
                {
                    ScanPanel.SetActive(true);
                }

                if (obj.name == "pCylinder45")
                {
                    ScanPanel.SetActive(true);
                }

                if (obj.name == "pCylinder32")
                {
                    ScanPanel.SetActive(true);
                }


                if (obj.name== "UpperKnob")
                {
                    obj.GetComponent<Animation>().Play();
                }

                if (obj.name == "UpperKnob1")
                {
                    obj.GetComponent<Animation>().Play();
                }

                if (obj.name == "UpperKnob2")
                {
                    obj.GetComponent<Animation>().Play();
                }

                if (obj.name == "LaunchBox")
                {
                    obj.GetComponent<Animation>().Play();
                }

                if (obj.name == "LaunchBox2")
                {
                    obj.GetComponent<Animation>().Play();
                }

                if (obj.name == "Launch")
                {
                    obj.GetComponent<Animation>().Play();
                }

                if (obj.name == "Launch1")
                {
                    obj.GetComponent<Animation>().Play();
                }
                //if (obj.name== "Button 1")
                //{
                //    obj.transform.Find("UpperKnob").GetComponent<Animation>().Play();
                //}
                Debug.Log(hit.collider.gameObject.name);
            }
        }
    }
}

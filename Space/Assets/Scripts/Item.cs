﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public bool isRightPos = false;      

    private GameObject mouseObject;
    // Start is called before the first frame update
    void Start()
    {
        mouseObject = GameObject.Find("mouse");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "position")
        {

            if (other.gameObject.name == mouseObject.GetComponent<Puzzle>().puzzlename)

                Debug.Log(mouseObject.GetComponent<Puzzle>().puzzlename);
                isRightPos = true;
            //this.transform.position = mouseObject.transform.position;
        }
    }
    
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "position")
        {
            if (other.gameObject.name == mouseObject.GetComponent<Puzzle>().puzzlename)
                isRightPos = false;
        }
    }
    
    void OnTriggerStay2D(Collider2D other)
    {
        Debug.Log(mouseObject.GetComponent<Puzzle>().puzzlename);
        if (other.gameObject.tag == "position")
        {
            if (other.gameObject.name == mouseObject.GetComponent<Puzzle>().puzzlename && !mouseObject.GetComponent<Puzzle>().isClicked)
            {
                this.transform.position = other.transform.position;
                this.transform.tag = "position";
            }
        }
    }
}

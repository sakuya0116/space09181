﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIBtn : MonoBehaviour
{
    public GameObject ScanPanel;
    public GameObject CollectionPanel;
    public GameObject PartsPanel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ScanBtn()
    {      
        ScanPanel.SetActive(false);
        PartsPanel.SetActive(true);     
    }

    public void PartsBtn()
    {
        PartsPanel.SetActive(false);
    }
}

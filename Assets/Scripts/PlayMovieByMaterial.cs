﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;


public class PlayMovieByMaterial : MonoBehaviour
{
    private RawImage rawImage;
    private int currentClipIndex;
    private VideoPlayer videoplayer;
    public Text text_play;
    public GameObject button_launch;
    public GameObject button_stage1;
    public GameObject button_stage2;
    public GameObject button_stage3;
    public VideoClip[] videoClips;

    void Start()
    {
      
        videoplayer = this.GetComponent<VideoPlayer>();
        rawImage = this.GetComponent<RawImage>();
        currentClipIndex = 0;
        //button_launch.onClick.AddListener(Launch);
        //button_stage1.onClick.AddListener(Stage1);
        //button_stage2.onClick.AddListener(Stage2);
        //button_stage3.onClick.AddListener(Stage3);


        /*videoPlayer.playOnAwake = false;

        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.CameraNearPlane;

        videoPlayer.targetCameraAlpha = 0.5f;

        videoPlayer.url = "1.mp4";

        GameObject.Find("stage1").GetComponent<Button>().onClick.AddListener(videoPlayer.Play(););
        videoPlayer.playbackSpeed = videoPlayer.playbackSpeed / 2F;

        videoPlayer.Play();*/

    }




    void Update()
    {
        if (videoplayer.texture == null)
        {
            return;
        }
        rawImage.texture = videoplayer.texture;
    }
    private void Launch()
    {
        videoplayer.clip = videoClips[1];

    }
    private void Stage1()
    {
        videoplayer.clip = videoClips[2];

    }
    private void Stage2()
    {
        
        videoplayer.clip = videoClips[3];


    }
    private void Stage3()
    {
        videoplayer.clip = videoClips[4];

    }

}
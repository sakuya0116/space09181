﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIBtn : MonoBehaviour
{
    public GameObject ScanPanel1;
    public GameObject ScanPanel2;
    public GameObject ScanPanel3;
    public GameObject ScanPanel4;

    public GameObject PropellorPanel;
    public GameObject FuelPanel1;
    public GameObject FuelPanel2;
    public GameObject RocketPanel;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ScanBtn1()
    {      
        ScanPanel1.SetActive(false);
        PropellorPanel.SetActive(true);     
    }

    public void ScanBtn2()
    {
        ScanPanel2.SetActive(false);
        FuelPanel1.SetActive(true);
    }

    public void ScanBtn3()
    {
        ScanPanel3.SetActive(false);
        FuelPanel2.SetActive(true);
    }

    public void ScanBtn4()
    {
        ScanPanel4.SetActive(false);
        RocketPanel.SetActive(true);
    }

    public void PartsBtn1()
    {
        PropellorPanel.SetActive(false);
    }

    public void PartsBtn2()
    {
        FuelPanel1.SetActive(false);
    }

    public void PartsBtn3()
    {
        FuelPanel2.SetActive(false);
    }

    public void PartsBtn4()
    {
        RocketPanel.SetActive(false);
    }
}

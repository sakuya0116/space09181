﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;

public class select : MonoBehaviour
{
    public Camera m_Camera;

    public GameObject ScanPanel1;
    public GameObject ScanPanel2;
    public GameObject ScanPanel3;
    public GameObject ScanPanel4;

    public RawImage rawImage;
    private int currentClipIndex;
    public VideoPlayer videoplayer;
    //public Text text_play;
    //public GameObject button_launch;
    //public GameObject button_stage1;
    //public GameObject button_stage2;
    //public GameObject button_stage3;
    public VideoClip[] videoClips;

    public Text CollectingProgress;
    int Collecting = 0;

    // Start is called before the first frame update
    void Start()
    {
        //videoplayer = this.GetComponent<VideoPlayer>();
        //rawImage = this.GetComponent<RawImage>();
        currentClipIndex = 0;

    }

    // Update is called once per frame
    void Update()
    {
        CollectingProgress.text = Collecting + " / 5 Parts";

        if (videoplayer.texture == null /*&& Collecting == 5*/)
        {
            return;
        }
        rawImage.texture = videoplayer.texture;

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = m_Camera.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            if (Physics.Raycast(ray, out hit))
            {
                GameObject obj = hit.collider.gameObject;
                if (obj.name == "pCylinder40")
                {
                    ScanPanel1.SetActive(true);
                    Collecting++;
                    obj.SetActive(false);
                }

                if (obj.name == "pCylinder45")
                {
                    ScanPanel3.SetActive(true);
                    Collecting++;
                    obj.SetActive(false);
                }

                if (obj.name == "pCylinder32")
                {
                    ScanPanel2.SetActive(true);
                    Collecting++;
                    obj.SetActive(false);
                }

                if (obj.name == "pCylinder50")
                {
                    ScanPanel1.SetActive(true);
                    Collecting++;
                    obj.SetActive(false);
                }

                if (obj.name == "pCylinder28")
                {
                    ScanPanel4.SetActive(true);
                    Collecting++;
                    obj.SetActive(false);
                }

                if (obj.name== "UpperKnob")
                {
                    obj.GetComponent<Animation>().Play();
                    Stage1();
                }

                if (obj.name == "UpperKnob1")
                {
                    obj.GetComponent<Animation>().Play();
                    Stage2();
                }

                if (obj.name == "UpperKnob2")
                {
                    obj.GetComponent<Animation>().Play();
                    Stage3();
                }

                if (obj.name == "LaunchBox")
                {
                    obj.GetComponent<Animation>().Play();
                }

                if (obj.name == "LaunchBox2")
                {
                    obj.GetComponent<Animation>().Play();
                }

                if (obj.name == "Launch")
                {
                    obj.GetComponent<Animation>().Play();
                    Launch();
                }

                if (obj.name == "Launch1")
                {
                    obj.GetComponent<Animation>().Play();
                    Launch();
                }
                //if (obj.name == "Cube")
                //{
                    
                //}
                
                Debug.Log(hit.collider.gameObject.name);
            }
        }
    }
    private void Launch()
    {
        videoplayer.clip = videoClips[1];

    }
    private void Stage1()
    {
        videoplayer.clip = videoClips[2];

    }
    private void Stage2()
    {

        videoplayer.clip = videoClips[3];


    }
    private void Stage3()
    {
        videoplayer.clip = videoClips[4];

    }

}

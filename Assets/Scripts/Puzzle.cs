﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle : MonoBehaviour
{
    public GameObject[] fragments;

    public string puzzlename = "position";

    public bool isMouseDown = false;
    //private bool isMouseDown = false;
    public bool isClicked = false;
    private bool overlap = false;
    private Vector3 oldposition;
    private GameObject target = null;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 temp = Vector3.zero;
        for (int i = 0; i < fragments.Length; i++)
        {
            int t = Random.Range(0, fragments.Length - 1);
            temp = fragments[i].transform.position;
            fragments[i].transform.position = fragments[t].transform.position;
            fragments[t].transform.position = temp;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1f));// wrong
        //transfer obj position into world position
        Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, pos.z);
        transform.position = Camera.main.ScreenToWorldPoint(mousePos);

        isMouseDown = Input.GetMouseButton(0);      
        if (!isMouseDown && isClicked)
        {   
            isClicked = false;
            if (!overlap)
                target.transform.position = oldposition;
        }
        
        if (isMouseDown && isClicked)
        {
            target.transform.position = transform.position;
            overlap = target.GetComponent<Item>().isRightPos;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (isMouseDown && !isClicked && other.gameObject.tag == "puzzle")
        {
            isClicked = true;
            oldposition = other.transform.position;
            target = GameObject.Find(other.gameObject.name);
            puzzlename = "position_" + other.gameObject.name[7];
        }
    }
}
